<?php

namespace Drupal\Tests\digital_wallet_server\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Functional tests for the digital_wallet_server module.
 *
 * @group digital_wallet_server
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class DigitalWalletMainWsUrlTest extends BrowserTestBase {

  /**
   * The admin user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'digital_wallet_server',
  ];

  /**
   * Permissions to grant admin user.
   *
   * @var array
   */
  protected $permissions = [
    'access administration pages',
    'administer digital wallet',
  ];

  /**
   * Sets the test up.
   */
  protected function setUp() {
    parent::setUp();
    // Test admin user.
    $this->adminUser = $this->drupalCreateUser($this->permissions);
  }

  /**
   * Test WS base URL setting.
   */
  public function testWsBaseUrl() {
    $assert_session = $this->assertSession();
    $this->drupalLogin($this->adminUser);

    $config_path = '/admin/config/digital-wallet/main-settings';
    // Fetch the Main Wallet API settings form.
    $this->drupalGet($config_path);

    $assert_session->statusCodeEquals(200);
  }

}
