# Module Translations

Add the module translation files here in the format `language-code.po`
such as `fr.po` for French translation.
