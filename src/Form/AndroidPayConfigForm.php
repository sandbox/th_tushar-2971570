<?php

namespace Drupal\digital_wallet_server\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form for Apple Wallet.
 */
class AndroidPayConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'digital_wallet_server_android_pay_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['digital_wallet_server.android_settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // @see https://www.drupal.org/node/2408549
    $android_wallet_config = \Drupal::config('digital_wallet_server.android_settings');

    $form['android_pay_api_settings'] = [
      '#type' => 'details',
      '#title' => t('Android Pay API settings'),
      '#open' => TRUE,
    ];

    $form['android_pay_api_settings']['service_account_email_address'] = [
      '#type' => 'textfield',
      '#title' => t('Service Account Email Address'),
      '#default_value' => $android_wallet_config->get('service_account_email_address'),
      '#description' => t('Android Developer Service account email address.'),
      '#required' => TRUE,
    ];

    $form['android_pay_api_settings']['issuer_id'] = [
      '#type' => 'textfield',
      '#title' => t('Issuer ID'),
      '#default_value' => $android_wallet_config->get('issuer_id'),
      '#description' => t('Android Developer Issue ID.'),
      '#required' => TRUE,
    ];

    $form['android_pay_api_settings']['service_account_private_key'] = [
      '#type' => 'textfield',
      '#title' => t('Private Key'),
      '#default_value' => $android_wallet_config->get('service_account_private_key'),
      '#description' => t('Path to Service Account private key (JSON file).'),
      '#required' => TRUE,
    ];

    $form['android_pay_api_settings']['application_name'] = [
      '#type' => 'textfield',
      '#title' => t('Application Name'),
      '#default_value' => $android_wallet_config->get('application_name'),
      '#description' => t('Application Name.'),
      '#required' => TRUE,
    ];

    $form['android_pay_api_settings']['origins'] = [
      '#type' => 'textfield',
      '#title' => t('Origins'),
      '#default_value' => $android_wallet_config->get('origins'),
      '#description' => t('Application origins for save to wallet button.'),
      '#required' => TRUE,
    ];

    $form['android_pay_api_settings']['save_to_android_pay'] = [
      '#type' => 'textfield',
      '#title' => t('Type of request (SAVE_TO_ANDROID_PAY)'),
      '#default_value' => $android_wallet_config->get('save_to_android_pay'),
      '#description' => t('Type of request. Defaults to "savetoandroidpay".'),
      '#required' => TRUE,
    ];

    $form['android_pay_api_settings']['loyalty_web'] = [
      '#type' => 'textfield',
      '#title' => t('Type of request (LOYALTY_WEB)'),
      '#default_value' => $android_wallet_config->get('loyalty_web'),
      '#description' => t('Type of request. Defaults to "loyaltywebservice".'),
      '#required' => TRUE,
    ];

    $form['android_pay_api_settings']['scopes'] = [
      '#type' => 'textfield',
      '#title' => t('Scopes'),
      '#default_value' => $android_wallet_config->get('scopes'),
      '#description' => t('Scopes of request. Defaults to "https://www.googleapis.com/auth/wallet_object.issuer".'),
      '#required' => TRUE,
    ];

    $form['android_pay_api_settings']['audience'] = [
      '#type' => 'textfield',
      '#title' => t('Audience'),
      '#default_value' => $android_wallet_config->get('audience'),
      '#description' => t('Audience of request. Defaults to "google".'),
      '#required' => TRUE,
    ];

    $form['android_pay_api_settings']['loyalty_class_id'] = [
      '#type' => 'textfield',
      '#title' => t('LOYALTY_CLASS_ID'),
      '#default_value' => $android_wallet_config->get('loyalty_class_id'),
      '#description' => t('LOYALTY_CLASS_ID of request. Defaults to "LoyaltyClass".'),
      '#required' => TRUE,
    ];

    $form['android_pay_api_settings']['loyalty_object_id'] = [
      '#type' => 'textfield',
      '#title' => t('LOYALTY_OBJECT_ID'),
      '#default_value' => $android_wallet_config->get('loyalty_object_id'),
      '#description' => t('LOYALTY_OBJECT_ID of request. Defaults to "LoyaltyObject".'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('digital_wallet_server.android_settings')
      ->set('service_account_email_address', $form_state->getValue('service_account_email_address'))
      ->set('issuer_id', $form_state->getValue('issuer_id'))
      ->set('service_account_private_key', $form_state->getValue('service_account_private_key'))
      ->set('application_name', $form_state->getValue('application_name'))
      ->set('origins', $form_state->getValue('origins'))
      ->set('save_to_android_pay', $form_state->getValue('save_to_android_pay'))
      ->set('loyalty_web', $form_state->getValue('loyalty_web'))
      ->set('scopes', $form_state->getValue('scopes'))
      ->set('audience', $form_state->getValue('audience'))
      ->set('loyalty_class_id', $form_state->getValue('loyalty_class_id'))
      ->set('loyalty_object_id', $form_state->getValue('loyalty_object_id'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
