<?php

namespace Drupal\digital_wallet_server\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form for Apple Wallet.
 */
class MainWalletConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'digital_wallet_server_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['digital_wallet_server.main_settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // @see https://www.drupal.org/node/2408549
    $main_wallet_config = \Drupal::config('digital_wallet_server.main_settings');

    // Get all apple wallet IDs.
    $wallet_entities = \Drupal::entityTypeManager()->getStorage('apple_wallet')->loadMultiple();
    $wallet_options = [];
    foreach ($wallet_entities as $value) {
      $wallet_options[$value->id()] = $value->label();
    }

    $form['main_wallet_api_settings'] = [
      '#type' => 'details',
      '#title' => t('Main Wallet API settings'),
      '#open' => TRUE,
    ];

    $form['main_wallet_api_settings']['webservice_base_url'] = [
      '#type' => 'textfield',
      '#title' => t('Webservice Base URL'),
      '#default_value' => $main_wallet_config->get('webservice_base_url'),
      '#description' => t('Webservice Base URL.'),
      '#required' => TRUE,
    ];

    $form['main_wallet_api_settings']['default_apple_wallet'] = [
      '#type' => 'select',
      '#title' => $this->t('Default Apple Wallet'),
      '#options' => $wallet_options,
      '#default_value' => $main_wallet_config->get('default_apple_wallet'),
      '#description' => t('Default Apple Wallet to be used when no wallet available.'),
      '#required' => TRUE,
    ];

    $form['main_wallet_api_settings']['omniture_reportsuite_id'] = [
      '#type' => 'textfield',
      '#title' => t('Omniture Report Suite ID'),
      '#default_value' => $main_wallet_config->get('omniture_reportsuite_id'),
      '#description' => t('Microservice Omniture Report Suite ID.'),
      '#required' => TRUE,
    ];

    $form['main_wallet_api_settings']['omniture_evar_data'] = [
      '#type' => 'textfield',
      '#title' => t('Value for eVar102'),
      '#default_value' => $main_wallet_config->get('omniture_evar_data'),
      '#description' => t('Microservice Omniture eVar102 value.'),
      '#required' => TRUE,
    ];

    $time_intervals = [
      '-1 day' => '-1 day',
      '-2 days' => '-2 days',
      '-3 days' => '-3 days',
      '-4 days' => '-4 days',
      '-5 days' => '-5 days',
      '-6 days' => '-6 days',
      '-1 week' => '-1 week',
      '-2 weeks' => '-2 weeks',
      '-3 weeks' => '-3 weeks',
      '-4 weeks' => '-4 weeks',
      '-5 weeks' => '-5 weeks',
      '-6 weeks' => '-6 weeks',
      '-1 month' => '-1 month',
      '-2 months' => '-2 months',
      '-3 months' => '-3 months',
      '-4 months' => '-4 months',
      '-5 months' => '-5 months',
      '-6 months' => '-6 months',
    ];

    $form['main_wallet_api_settings']['wallet_request_data_reset_interval'] = [
      '#type' => 'select',
      '#title' => $this->t('Clearing Wallet Requests data'),
      '#options' => $time_intervals,
      '#default_value' => $main_wallet_config->get('wallet_request_data_reset_interval'),
      '#description' => t('Clearing wallet request data on cron run.'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('digital_wallet_server.main_settings')
      ->set('webservice_base_url', $form_state->getValue('webservice_base_url'))
      ->set('default_apple_wallet', $form_state->getValue('default_apple_wallet'))
      ->set('omniture_reportsuite_id', $form_state->getValue('omniture_reportsuite_id'))
      ->set('omniture_evar_data', $form_state->getValue('omniture_evar_data'))
      ->set('wallet_request_data_reset_interval', $form_state->getValue('wallet_request_data_reset_interval'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
