<?php

namespace Drupal\digital_wallet_server\Controller;

use Drupal\Component\Utility\SafeMarkup;
use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Controller for building a listing of Wallet Layout.
 */
class DigitalAppleWalletSettingsListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['title'] = $this->t('Title');
    $header['created'] = $this->t('Created');
    $header['changed'] = $this->t('Last Modified');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['title'] = SafeMarkup::checkPlain($entity->title);
    $row['created'] = $entity->created ? date('d-m-Y H:i:s', $entity->created) : '';
    $row['changed'] = $entity->updated ? date('d-m-Y H:i:s', $entity->updated) : '';

    return $row + parent::buildRow($entity);
  }

}
