<?php

namespace Drupal\digital_wallet_server\Controller;

use android_pay_s2ap\AndroidUtils\WobPayload;
use android_pay_s2ap\AndroidUtils\WobUtils;
use Drupal\Core\Controller\ControllerBase;
use Drupal\digital_wallet_server\Entity\WalletConsumer;
use Drupal\digital_wallet_server\StatusCodes;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controller routines for user routes.
 */
class DigitalWalletController extends ControllerBase {

  /**
   * Controller for providing the Wallet links.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON object containing Apple and Android wallet links.
   *
   * @throws \Exception
   */
  public function getWalletLinks(Request $request) {
    $response = [];

    $request_content_json = $request->get('data');
    if (empty($request_content_json)) {
      $request_content_json = $request->getContent();
    }

    $config = \Drupal::config('digital_wallet_server.main_settings');
    $ws_url = $config->get('webservice_base_url');

    if (!empty($request_content_json)) {
      $request_content = \GuzzleHttp\json_decode($request_content_json);

      try {
        if (isset($request_content->uuid) && $this->uuidExists($request_content->uuid)) {
          $uuid = $request_content->uuid;

          \Drupal::database()->update('digital_wallet_server_requests')
            ->fields([
              'data' => $request_content_json,
            ])
            ->condition('uuid', $uuid)
            ->execute();
        }
        else {
          $uuid_service = \Drupal::service('uuid');
          $uuid = $uuid_service->generate();

          \Drupal::database()->insert('digital_wallet_server_requests')
            ->fields([
              'uuid' => $uuid,
              'data' => $request_content_json,
              'timestamp' => time(),
            ])->execute();
        }
      }
      catch (Exception $e) {
        \Drupal::logger('digital_wallet_server')->error(print_r($e, TRUE));
      }

      $valid_consumer = $this->validateConsumer($request_content->brand_data->id, $request_content->brand_data->base_url);

      if ($valid_consumer instanceof JsonResponse) {
        return $valid_consumer;
      }

      if (isset($request_content->apple)) {
        if (!empty($request_content->apple->data)) {
          $response['apple_download_link'] = $ws_url . '/apple_pkpass/' . $uuid;
        }
      }

      if (isset($request_content->android)) {
        $android_data = $request_content->android->data->data;

        if (!empty($android_data)) {
          $wobPayload = new WobPayload($android_data->origin);

          if ($android_data->objecttype == 'LoyaltyObject') {
            $google_loyalty_service = \Drupal::service('digital_wallet_server.android_loyalty');

            $android_object = $google_loyalty_service->generateLoyaltyObject($android_data->classid, $android_data->objectid, $android_data);
          }

          $wobPayload->addWalletObjects($android_object, $android_data->objecttype);

          // Save to wallet request body.
          $requestBody = $wobPayload->getSaveToWalletRequest();
          // Create the response JWT.
          $utils = new WobUtils();

          $google_client_service = \Drupal::service('digital_wallet_server.androidclient');
          $client = $google_client_service->getClient();

          $jwt = $utils->makeSignedJwt($requestBody, $client);

          $response['android_pay_jwt'] = $jwt;
          $response['android_analytic_link'] = $ws_url . '/android_analytics/' . $uuid;
        }
      }
      $response['uuid'] = $uuid;
      $status = StatusCodes::HTTP_OK;
    }
    else {
      $response['status'] = $status = StatusCodes::HTTP_NOT_FOUND;
      $response['data'] = $this->t('Request data not found.');
    }

    return new JsonResponse($response, $status);
  }

  /**
   * Controller for generating and downloading the '.pkpass' file.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   * @param string $uuid
   *   Unique pass identifier.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Generates and downloads Apple PKPass file.
   */
  public function downloadPkPass(Request $request, $uuid = '') {
    try {
      $result = \Drupal::database()->select('digital_wallet_server_requests', 're')
        ->fields('re', [])
        ->condition('uuid', $uuid)
        ->execute()->fetchAssoc();
    }
    catch (Exception $e) {
      \Drupal::logger('digital_wallet_server')->error(print_r($e, TRUE));
    }

    $data = $result['data'];
    $request_content_apple = \GuzzleHttp\json_decode($data);
    $request_content = $request_content_apple->apple;
    $consumer = $this->checkEntity('wallet_consumer', $request_content_apple->brand_data->id);

    if (!empty($consumer) && !empty($request_content)) {
      $apple_wallet = $this->checkEntity('apple_wallet', $consumer->apple_wallet->value);

      if (empty($apple_wallet)) {
        $default_apple_wallet_id = \Drupal::config('digital_wallet_server.main_settings')->get('default_apple_wallet');
        $apple_wallet = \Drupal::entityManager()->getStorage('apple_wallet')->load($default_apple_wallet_id);

        if (empty($apple_wallet)) {
          return new JsonResponse(NULL, StatusCodes::HTTP_NOT_FOUND);
        }
      }

      $pkpass_service = \Drupal::service('digital_wallet_server.applepkpass');
      $pkpass_service->initialize($apple_wallet->id());

      $request_content->data->formatVersion = (int) $apple_wallet->format_version->value;
      $request_content->data->passTypeIdentifier = $apple_wallet->pass_type_identifier->value;
      $request_content->data->teamIdentifier = $apple_wallet->team_identifier->value;
      $request_content->data->organizationName = $apple_wallet->organization_name->value;

      $request_data = \GuzzleHttp\json_encode($request_content->data);

      $pkpass_service->setData($request_data);

      $files = (array) $request_content->files;
      foreach ($files as $id => $file) {
        $pkpass_service->addRemoteFile($file, $id . '.png');
      }

      // Analytics code goes here.
      $brand_data = (array) $request_content_apple->brand_data;
      $brand_data['id'] = $consumer->report_suite_id;
      $analytics_reponse = $this->generateAnalyticsXml($brand_data, 'event189');

      if (!$pkpass_service->create(TRUE)) {
        return new JsonResponse('Error: ' . $pkpass_service->getError(), StatusCodes::HTTP_INTERNAL_SERVER_ERROR);
      }
    }

    return new JsonResponse(NULL, StatusCodes::HTTP_NOT_FOUND);
  }

  /**
   * Controller for inserting the Android Class on Google Server.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON object containing response and status.
   */
  public function androidClassInsert(Request $request) {
    $response = [];
    $status = 404;
    $config = \Drupal::config('digital_wallet_server.android_settings');

    $request_content_json = $request->get('data');
    if (empty($request_content_json)) {
      $request_content_json = $request->getContent();
    }

    if (!empty($request_content_json)) {
      $google_client_service = \Drupal::service('digital_wallet_server.androidclient');
      $google_loyalty_service = \Drupal::service('digital_wallet_server.android_loyalty');
      $client = $google_client_service->getClient();
      $service = new \Google_Service_Walletobjects($client);

      $data = \GuzzleHttp\json_decode($request_content_json);

      try {
        $loyalty_class = $google_loyalty_service->generateLoyaltyClass($data->data->id, (array) $data);
        $update_status = $service->loyaltyclass->insert($loyalty_class);

        $response['response'] = $update_status;
        $response['status'] = $status = StatusCodes::HTTP_OK;
      }
      catch (\Google_Service_Exception $e) {
        $errors = $e->getErrors();

        $response['status'] = $status = StatusCodes::HTTP_INTERNAL_SERVER_ERROR;
        $response['reason'] = $errors['0']['reason'];
      }
    }

    return new JsonResponse($response, $status);
  }

  /**
   * Controller for updating the Android Class on Google Server.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON object containing response and status.
   */
  public function androidClassUpdate(Request $request) {
    $response = [];
    $status = 404;
    $config = \Drupal::config('digital_wallet_server.android_settings');

    $request_content_json = $request->get('data');
    if (empty($request_content_json)) {
      $request_content_json = $request->getContent();
    }

    if (!empty($request_content_json)) {
      $google_client_service = \Drupal::service('digital_wallet_server.androidclient');
      $google_loyalty_service = \Drupal::service('digital_wallet_server.android_loyalty');
      $client = $google_client_service->getClient();
      $service = new \Google_Service_Walletobjects($client);

      $data = \GuzzleHttp\json_decode($request_content_json);

      try {
        $loyalty_class = $google_loyalty_service->generateLoyaltyClass($data->data->id, (array) $data);
        $update_status = $service->loyaltyclass->update($config->get('issuer_id') . '.' . $data->data->id, $loyalty_class);

        $response['response'] = $update_status;
        $response['status'] = $status = StatusCodes::HTTP_OK;
      }
      catch (\Google_Service_Exception $e) {
        $errors = $e->getErrors();

        $response['status'] = $status = StatusCodes::HTTP_BAD_REQUEST;
        $response['reason'] = $errors['0']['reason'];
      }
    }

    return new JsonResponse($response, $status);
  }

  /**
   * Controller for validating the Android Class on Google Server.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON object containing response and status.
   */
  public function androidClassValidate(Request $request) {
    $response = [];
    $status = 404;
    $config = \Drupal::config('digital_wallet_server.android_settings');

    $request_content_json = $request->get('data');
    if (empty($request_content_json)) {
      $request_content_json = $request->getContent();
    }

    if (empty($request_content_json)) {
      $google_client_service = \Drupal::service('digital_wallet_server.androidclient');
      $client = $google_client_service->getClient();
      $service = new \Google_Service_Walletobjects($client);
      $response['valid'] = FALSE;

      try {
        $loyaltyObj = $service->loyaltyclass->get($config->get('issuer_id') . '.' . $request_content_json);
        if ($loyaltyObj instanceof \Google_Service_Walletobjects_LoyaltyClass) {
          $response['valid'] = FALSE;
        }
      }
      catch (\Google_Service_Exception $e) {
        $response['valid'] = TRUE;
      }

      $status = 200;
    }

    return new JsonResponse($response, $status);
  }

  /**
   * Controller for capturing the Analytics Data.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   * @param string $uuid
   *   Unique pass identifier.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Response received from the Adobe server.
   */
  public function androidAnalyticInsert(Request $request, $uuid = '') {
    try {
      $result = \Drupal::database()->select('digital_wallet_server_requests', 're')
        ->fields('re', [])
        ->condition('uuid', $uuid)
        ->execute()->fetchAssoc();
    }
    catch (Exception $e) {
      \Drupal::logger('digital_wallet_server')->error(print_r($e, TRUE));
    }

    $data = $result['data'];
    $status = StatusCodes::HTTP_NOT_FOUND;

    $request_content_android = \GuzzleHttp\json_decode($data);
    $request_content = $request_content_android->android;

    $consumer = $this->checkEntity('walletconsumer', $request_content_android->brand_data->id);

    if (!empty($consumer) && !empty($request_content)) {
      // Analytics code goes here.
      $brand_data = (array) $request_content_android->brand_data;
      $brand_data['id'] = $consumer->analytics_sitename;
      $analytics_reponse = $this->generateAnalyticsXml($brand_data, 'event190');

      $status = StatusCodes::HTTP_OK;

      return new JsonResponse($analytics_reponse, $status);
    }

    return new JsonResponse(NULL, $status);
  }

  /**
   * Controller for processing the data and passing to adobe server.
   *
   * @param array $brand_data
   *   Entire brand data recieved.
   * @param int $event_id
   *   Event indentifier for Apple or Android event.
   *
   * @return reponse
   *   Response received from the Adobe server.
   */
  public function generateAnalyticsXml(array $brand_data, $event_id = NULL) {
    $config = \Drupal::config('digital_wallet_server.main_settings');
    $omniture_reportsuite_id = $config->get('omniture_reportsuite_id');
    $omniture_evar_data = $config->get('omniture_evar_data');

    if (!empty($omniture_reportsuite_id)) {
      $xml = xmlwriter_open_memory();
      xmlwriter_set_indent($xml, 1);
      $res = xmlwriter_set_indent_string($xml, ' ');

      xmlwriter_start_document($xml, '1.0', 'UTF-8');
      // A first element.
      xmlwriter_start_element($xml, 'request');
      xmlwriter_start_element($xml, 'scXmlVer');
      xmlwriter_text($xml, '1.0');
      xmlwriter_end_element($xml);

      xmlwriter_start_element($xml, 'reportSuiteID');
      xmlwriter_text($xml, $omniture_reportsuite_id);
      xmlwriter_end_element($xml);

      xmlwriter_start_element($xml, 'ipAddress');
      xmlwriter_text($xml, $brand_data['ip']);
      xmlwriter_end_element($xml);

      xmlwriter_start_element($xml, 'pageURL');
      xmlwriter_text($xml, $brand_data['url']);
      xmlwriter_end_element($xml);

      xmlwriter_start_element($xml, 'pageName');
      xmlwriter_text($xml, $brand_data['title']);
      xmlwriter_end_element($xml);

      xmlwriter_start_element($xml, 'eVar101');
      xmlwriter_text($xml, $brand_data['id']);
      xmlwriter_end_element($xml);

      if (!empty($omniture_evar_data)) {
        xmlwriter_start_element($xml, 'eVar102');
        xmlwriter_text($xml, $omniture_evar_data);
        xmlwriter_end_element($xml);
      }

      xmlwriter_start_element($xml, 'events');
      xmlwriter_text($xml, $event_id);
      xmlwriter_end_element($xml);

      // End Request Tag.
      xmlwriter_end_element($xml);

      xmlwriter_end_document($xml);

      $xml_string = xmlwriter_output_memory($xml);

      // Create POST, Host and Content-Length headers.
      $head = "POST /b/ss//6 HTTP/1.0\n";
      $head .= "Host: sc.omtrdc.net\n";
      $head .= "Content-Length: " . (string) strlen($xml_string) . "\n\n";

      // Combine the head and XML.
      $request = $head . $xml_string;

      $fp = fsockopen('sc.omtrdc.net', 80, $errno, $errstr, 30);
      if ($fp) {
        // Send data.
        fwrite($fp, $request);
        // Get response.
        $response = "";
        while (!feof($fp)) {
          $response .= fgets($fp, 1028);
        }
        fclose($fp);
        // Check for errors.
        if (preg_match("/status\>FAILURE\<\/status/im", $response)) {
          \Drupal::logger('digital_wallet_server')->error('FAILURE to post on adobe analytics.');
        }
      }
      else {
        \Drupal::logger('digital_wallet_server')->error('Couldn\'t open port to Analytics servers.');
      }

      return $response;
    }
  }

  /**
   * Controller for processing the data and passing to adobe server.
   *
   * @param string $entity_type
   *   Entity type to be loaded.
   * @param int $id
   *   Entity ID to be loaded.
   *
   * @return reponse
   *   Return the loaded entity.
   */
  public function checkEntity($entity_type, $id) {
    $entity_manager = \Drupal::entityManager();
    $entity = $entity_manager->getStorage($entity_type)->load($id);

    if (!empty($entity)) {
      return $entity;
    }
    else {
      \Drupal::logger('digital_wallet_server')->error(t('Entity: @entity_type with ID: @id not found.', ['@entity_type' => $entity_type, '@id' => $id]));
      return FALSE;
    }
  }

  /**
   * Checks if request uuid already exists.
   *
   * @param string $uuid
   *   UUID for Request Object.
   *
   * @return bool
   *   Return true if UUID already exists.
   */
  public function uuidExists($uuid = NULL) {
    $results = \Drupal::database()->select('digital_wallet_server_requests', 're')
      ->fields('re', ['uuid'])
      ->condition('uuid', $uuid)
      ->execute()->fetchAll();

    return !empty($results) ? TRUE : FALSE;
  }

  /**
   * Function to validate Wallet Consumer.
   *
   * @param string $consumer_id
   *   Consumer ID.
   * @param string $brand_base_url
   *   Brand Site Base URL or Consumer Domain.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Return message and status code if consumer is not valid.
   */
  public function validateConsumer($consumer_id, $brand_base_url) {
    if (isset($consumer_id)) {
      $consumer = \Drupal::entityManager()
        ->getStorage('wallet_consumer')->load($consumer_id);

      if ($consumer instanceof WalletConsumer) {
        if (!$consumer->status->value) {
          \Drupal::logger('digital_wallet_server')->error('The subscription having title "%title" is currently blocked.', [
            '%title' => $consumer->label(),
          ]);

          $response['status'] = $status = StatusCodes::HTTP_FORBIDDEN;
          $response['data'] = $this->t('Consumer ID is currently blocked.');

          return new JsonResponse($response, $status);
        }

        $consumer_domains_config = $consumer->domains->value;
        $consumer_domains = explode('\n', $consumer_domains_config);

        if (!empty($consumer_domains)) {
          if (!in_array($brand_base_url, $consumer_domains)) {
            \Drupal::logger('digital_wallet_server')->error('The subscription having title "%title" is not matching with request domain.', [
              '%title' => $consumer->label(),
            ]);

            $response['status'] = $status = StatusCodes::HTTP_FORBIDDEN;
            $response['data'] = $this->t('Wallet request is not permitted from current domain.');

            return new JsonResponse($response, $status);
          }
        }
        else {
          $response['status'] = $status = StatusCodes::HTTP_FORBIDDEN;
          $response['data'] = $this->t('Domains are not assigned for current wallet consumer.');

          return new JsonResponse($response, $status);
        }
      }
      else {
        $response['status'] = $status = StatusCodes::HTTP_NOT_FOUND;
        $response['data'] = $this->t('Consumer ID not found.');

        return new JsonResponse($response, $status);
      }
    }
    else {
      $response['status'] = $status = StatusCodes::HTTP_FORBIDDEN;
      $response['data'] = $this->t('Consumer ID cannot be empty.');

      return new JsonResponse($response, $status);
    }
  }

}
