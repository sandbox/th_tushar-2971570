<?php

namespace Drupal\digital_wallet_server\Controller;

use Drupal\Component\Utility\SafeMarkup;
use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Controller for building a listing of Wallet Layout.
 */
class DigitalWalletConsumerListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['title'] = $this->t('Title');
    $header['domain'] = $this->t('Domain');
    $header['apple_wallet_id'] = $this->t('Apple Wallet');
    $header['created'] = $this->t('Created');
    $header['changed'] = $this->t('Last Modified');
    $header['status'] = $this->t('Active / Blocked');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['title'] = SafeMarkup::checkPlain($entity->title);
    $row['domain'] = SafeMarkup::checkPlain($entity->domain);
    $row['apple_wallet_id'] = SafeMarkup::checkPlain($entity->apple_wallet_id);
    $row['created'] = $entity->created ? date('d-m-Y H:i:s', $entity->created) : '';
    $row['changed'] = $entity->updated ? date('d-m-Y H:i:s', $entity->updated) : '';
    $row['status'] = $entity->get('status') ? $this->t('Active') : $this->t('Blocked');

    return $row + parent::buildRow($entity);
  }

}
