<?php

namespace Drupal\digital_wallet_server;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Interface AppleWalletInterface.
 *
 * @package Drupal\digital_wallet_server
 */
interface AppleWalletInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}
