<?php

namespace Drupal\digital_wallet_server;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityManagerInterface;

/**
 * Class WalletConsumer.
 *
 * @package Drupal\digital_wallet_server
 */
class WalletConsumer {

  /**
   * Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   *   Config Factory.
   */
  protected $configFactory;

  /**
   * Entity Manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   *   Entity Manager.
   */
  protected $entityManager;

  /**
   * Wallet Consumer constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   Config Factory.
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   Entity Manager.
   */
  public function __construct(ConfigFactory $configFactory, EntityManagerInterface $entity_manager) {
    $this->configFactory = $configFactory;
    $this->entityManager = $entity_manager;
  }

  /**
   * Verify and return consumer.
   *
   * @param int $id
   *   Entity ID.
   * @param string $username
   *   User name.
   * @param string $password
   *   Password.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   Consumer Entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function verify($id, $username, $password) {
    $consumer = $this->entityManager->getStorage('walletconsumer')->load($id);

    return $consumer;
  }

}
