<?php

namespace Drupal\digital_wallet_server;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\user\Entity\User;

/**
 * Class BrandOnboarding.
 */
class BrandOnboarding {

  /**
   * Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   *   Config Factory.
   */
  protected $configFactory;

  /**
   * Entity Manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   *   Entity Manager.
   */
  protected $entityManager;

  /**
   * Wallet Consumer constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   Config Factory.
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   Entity Manager.
   */
  public function __construct(ConfigFactory $configFactory, EntityManagerInterface $entity_manager) {
    $this->configFactory = $configFactory;
    $this->entityManager = $entity_manager;
  }

  /**
   * Function to onboard a brand site.
   *
   * @param array $brand_data
   *   An array of Brand's data.
   */
  public function onboardBrand(array $brand_data) {
    // To create user for OAuth consumer.
    $username = $brand_data['username'];
    $email = $brand_data['email'];
    $user_id = $this->createUser($username, $email);

    // To create OAuth Consumer.
    $consumer_label = $brand_data['consumer_label'];
    $oauth_consumer_id = $this->createOauthConsumer($consumer_label, $user_id);

    // To create Wallet Consumer.
    $wallet_consumer_data = [
      'id' => $brand_data['wallet_consumer_id'],
      'title' => $brand_data['wallet_consumer_title'],
      'domain' => $brand_data['wallet_consumer_domain'],
      'apple_wallet_id' => $brand_data['wallet_consumer_apple_id'],
      'analytics_sitename' => $brand_data['analytics_sitename'],
      'uid' => \Drupal::currentUser()->id(),
      'oauth_consumer_id' => $oauth_consumer_id,
      'created' => time(),
      'updated' => '',
    ];

    $wallet_consumer = $this->createWalletConsumer($wallet_consumer_data);

    \Drupal::logger('digital_wallet_server')->notice('Wallet consumer with title %title has been created successfully.', [
      '%title' => $wallet_consumer_data['title'],
    ]);
  }

  /**
   * Creates an OAuth user.
   *
   * @param string $username
   *   User name.
   * @param string $email
   *   User's email.
   *
   * @return int
   *   User ID.
   */
  public function createUser($username, $email) {
    if ($uid = $this->checkUserByName($username)) {
      return $uid;
    }
    elseif ($uid = $this->checkUserByEmail($email)) {
      return $uid;
    }
    else {
      // Create user with brand consumer role.
      $user = User::create();
      $user->setUsername($username);
      $user->setPassword($username . "123");
      $user->setEmail($email);
      $user->addRole('brand_consumer');
      $user->enforceIsNew();
      $user->activate();

      $user->save();

      return $user->id();
    }
  }

  /**
   * Creates OAuth Consumer.
   *
   * @param string $label
   *   Consumer Label.
   * @param int $user
   *   User ID.
   *
   * @return int
   *   Consumer ID.
   */
  public function createOauthConsumer($label, $user) {
    // Create a consumer for OAuth.
    $consumer_data = [
      'owner_id' => 1,
      'label' => $label,
      'description' => 'An OAuth consumer for brand site to consume digital wallet microservice.',
      'image' => NULL,
      'user_id' => $user,
      'secret' => '123456',
      'third_party' => FALSE,
      'roles' => ['brand_consumer'],
    ];
    $consumer = $this->entityManager
      ->getStorage('consumer')
      ->create($consumer_data);
    $consumer->save();

    return $consumer->id();
  }

  /**
   * Creates Wallet Consumer.
   *
   * @param array $consumer_data
   *   Data for consumer entity.
   */
  public function createWalletConsumer(array $consumer_data) {
    $consumer = $this->entityManager
      ->getStorage('walletconsumer')
      ->create($consumer_data);

    $consumer->save();
  }

  /**
   * Updates Wallet Consumer.
   *
   * @param string $consumer_id
   *   Consumer ID.
   * @param array $consumer_data
   *   Consumer Data.
   */
  public function updateWalletConsumer($consumer_id, array $consumer_data) {
    $consumer = $this->entityManager
      ->getStorage('walletconsumer')->load($consumer_id);

    if (isset($consumer_data['title'])) {
      $consumer->title = $consumer_data['title'];
    }

    if (isset($consumer_data['domain'])) {
      $consumer->title = $consumer_data['domain'];
    }

    if (isset($consumer_data['apple_wallet_id'])) {
      $consumer->apple_wallet_id = $consumer_data['apple_wallet_id'];
    }

    if (isset($consumer_data['analytics_sitename'])) {
      $consumer->analytics_sitename = $consumer_data['analytics_sitename'];
    }

    if (isset($consumer_data['oauth_consumer_id'])) {
      $consumer->oauth_consumer_id = $consumer_data['oauth_consumer_id'];
    }

    $consumer->save();
  }

  /**
   * Check user by name.
   *
   * @param string $username
   *   User name.
   *
   * @return int
   *   User ID or false it do not exists.
   */
  public function checkUserByName($username = NULL) {
    $user_exists = FALSE;
    if ($username != NULL) {
      $user = user_load_by_name($username);
      if ($user instanceof User) {
        $user_exists = $user->id();
      }
    }

    return $user_exists;
  }

  /**
   * Check user by email.
   *
   * @param string $email
   *   Email.
   *
   * @return int
   *   User ID or false it do not exists.
   */
  public function checkUserByEmail($email) {
    $user_exists = FALSE;
    if ($email != NULL) {
      $user = user_load_by_mail($email);
      if ($user instanceof User) {
        $user_exists = $user->id();
      }
    }

    return $user_exists;
  }

}
