<?php

namespace Drupal\digital_wallet_server\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\digital_wallet_server\ConsumerEntityInterface;

/**
 * Defines the Apple Wallet Setting entity.
 *
 * @ConfigEntityType(
 *   id = "applewallet",
 *   label = @Translation("Apple Wallet"),
 *   handlers = {
 *     "list_builder" = "Drupal\digital_wallet_server\Controller\DigitalAppleWalletSettingsListBuilder",
 *     "form" = {
 *       "add" = "Drupal\digital_wallet_server\Form\AppleWalletSettingsForm",
 *       "edit" = "Drupal\digital_wallet_server\Form\AppleWalletSettingsForm",
 *       "delete" = "Drupal\digital_wallet_server\Form\AppleWalletSettingsDeleteForm",
 *     }
 *   },
 *   config_prefix = "applewallet",
 *   admin_permission = "administer digital wallet",
 *   entity_keys = {
 *     "id" = "id",
 *   },
 *   links = {
 *     "edit-form" = "/admin/config/digital-wallet/apple-setting/{applewallet}/edit",
 *     "delete-form" = "/admin/config/digital-wallet/apple-setting/{applewallet}/delete",
 *   }
 * )
 */
class AppleWalletSettings extends ConfigEntityBase implements ConsumerEntityInterface {

  /**
   * The Apple Wallet Setting ID.
   *
   * @var string
   */
  public $id;

  /**
   * The Apple Wallet Setting label.
   *
   * @var string
   */
  public $title;

  /**
   * Path to Apple WWDR Certificate file..
   *
   * @var string
   */
  public $apple_wwdr_file_path;

  /**
   * Path to developer account certificate file (.p12).
   *
   * @var string
   */
  public $apple_p12_file_path;

  /**
   * Password related to developer account certificate file (.p12).
   *
   * @var string
   */
  public $apple_cert_file_pass;

  /**
   * Format Version of the coupon in wallet.
   *
   * @var int
   */
  public $apple_formatversion;

  /**
   * Pass Type Identifier of the coupon in wallet.
   *
   * @var string
   */
  public $apple_passtypeid;

  /**
   * Team Identifier of the coupon in wallet.
   *
   * @var string
   */
  public $apple_teamid;

  /**
   * Organization Name of the coupon in wallet.
   *
   * @var string
   */
  public $apple_orgname;

  /**
   * The Apple Wallet Setting user's UID.
   *
   * @var string
   */
  public $uid;

  /**
   * The Apple Wallet Setting creation date.
   *
   * @var string
   */
  public $created;

  /**
   * The Apple Wallet Setting updated date.
   *
   * @var string
   */
  public $updated;

}
