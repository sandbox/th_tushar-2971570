<?php

namespace Drupal\digital_wallet_server\Entity\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Class WalletConsumerListBuilder.
 *
 * @package Drupal\digital_wallet_server\Entity\Controller
 */
class WalletConsumerListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   *
   * We override ::render() so that we can add our own content above the table.
   * parent::render() is where EntityListBuilder creates the table using our
   * buildHeader() and buildRow() implementations.
   */
  public function render() {
    $build['description'] = [
      '#markup' => $this->t('These Apple Wallet settings are fieldable entities. You can manage the fields on the <a href="@adminlink">Wallet Consumer Setttings page</a>.', [
        '@adminlink' => \Drupal::urlGenerator()
          ->generateFromRoute('digital_wallet_server.wallet_consumer_settings'),
      ]),
    ];

    $build += parent::render();
    return $build;
  }

  /**
   * {@inheritdoc}
   *
   * Building the header and content lines for the contact list.
   *
   * Calling the parent::buildHeader() adds a column for the possible actions
   * and inserts the 'edit' and 'delete' links as defined for the entity type.
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['name'] = $this->t('Name');
    $header['status'] = $this->t('Status');
    $header['created'] = $this->t('Created');
    $header['changed'] = $this->t('Updated');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['id'] = $entity->id();
    $row['name'] = $entity->link();
    $row['status'] = $entity->get('status')->value ? $this->t('Enabled') : $this->t('Disabled');
    $row['created'] = $entity->get('created')->value ? date('d-m-Y H:i:s', $entity->get('created')->value) : '';
    $row['changed'] = $entity->get('changed')->value ? date('d-m-Y H:i:s', $entity->get('changed')->value) : '';

    return $row + parent::buildRow($entity);
  }

}
