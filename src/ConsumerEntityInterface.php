<?php

namespace Drupal\digital_wallet_server;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining an Wallet Consumer entity.
 */
interface ConsumerEntityInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}
