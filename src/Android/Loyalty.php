<?php

namespace Drupal\digital_wallet_server\Android;

use Drupal\Core\Config\ConfigFactory;
use Google_Service_Walletobjects_Barcode;
use Google_Service_Walletobjects_Image;
use Google_Service_Walletobjects_LinksModuleData;
use Google_Service_Walletobjects_LoyaltyClass;
use Google_Service_Walletobjects_Uri;

/**
 * Class to generate example Loyalty class and objects.
 */
class Loyalty {

  /**
   * Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * ApplePKPass constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   Config Factory.
   */
  public function __construct(ConfigFactory $configFactory) {
    $this->configFactory = $configFactory;
  }

  /**
   * Generates a Loyalty Class.
   *
   * @param string $class_id
   *   Wallet Class that this wallet object references.
   * @param string $data
   *   Data to be sent to Google server.
   *
   * @return object
   *   Loyaltyclass resource.
   */
  public function generateLoyaltyClass($class_id, $data) {
    $data = (array) $data['data'];

    $config = $this->configFactory->get('digital_wallet_server.android_settings');
    $issuer_id = $config->get('issuer_id');

    // Source uri of program logo.
    $uri_instance = new Google_Service_Walletobjects_Uri();
    $image_instance = new Google_Service_Walletobjects_Image();
    $uri_instance->setUri(
      $data['program_logo']
    );
    $image_instance->setSourceUri($uri_instance);

    $hero_uri_instance = new Google_Service_Walletobjects_Uri();
    $heroimage_instance = new Google_Service_Walletobjects_Image();
    $hero_uri_instance->setUri(
      $data['hero_image']
    );
    $heroimage_instance->setSourceUri($hero_uri_instance);

    // Create wallet class.
    $wob_class = new Google_Service_Walletobjects_LoyaltyClass();
    $wob_class->setId($issuer_id . '.' . $class_id);
    $wob_class->setIssuerName($data['issuer_name']);
    $wob_class->setProgramName($data['program_name']);
    $wob_class->setHexBackgroundColor($data['backgroundcolor']);
    $wob_class->setProgramLogo($image_instance);
    $wob_class->setHeroImage($heroimage_instance);

    // Reward Tier Label.
    if (!empty($data['reward_tier_label'])) {
      $wob_class->setRewardsTierLabel($data['reward_tier_label']);
    }

    // Reward Tier Value.
    if (!empty($data['reward_tier_value'])) {
      $wob_class->setRewardsTier($data['reward_tier_value']);
    }

    // Account Name Label.
    if (!empty($data['account_name_label'])) {
      $wob_class->setAccountNameLabel($data['account_name_label']);
    }

    // Account ID Label.
    if (!empty($data['account_id_label'])) {
      $wob_class->setAccountIdLabel($data['account_id_label']);
    }

    $wob_class->setReviewStatus('underReview');
    $wob_class->setAllowMultipleUsersPerObject(TRUE);

    return $wob_class;
  }

  /**
   * Generates a Loyalty Object.
   *
   * @param string $class_id
   *   Wallet Class that this wallet object references.
   * @param string $object_id
   *   Unique identifier for a wallet object.
   * @param object $data
   *   Data related to this object.
   *
   * @return object
   *   Loyaltyobject resource.
   */
  public function generateLoyaltyObject($class_id, $object_id, $data) {
    $data = (array) $data;

    $config = $this->configFactory->get('digital_wallet_server.android_settings');
    $issuer_id = $config->get('issuer_id');

    // Define links module data.
    $links_module_data = new Google_Service_Walletobjects_LinksModuleData();
    $links = (array) $data['links'];
    $uris[] = [
      'uri' => $links['0']->uri,
      'kind' => 'walletobjecs#uri',
      'description' => $links['0']->description,
    ];

    if (!empty($links['1']->uri) && !empty($links['1']->description)) {
      $uris[] = [
        'uri' => $links['1']->uri,
        'kind' => 'walletobjecs#uri',
        'description' => $links['1']->description,
      ];
    }

    if (!empty($links['2']->uri) && !empty($links['2']->description)) {
      $uris[] = [
        'uri' => $links['2']->uri,
        'kind' => 'walletobjecs#uri',
        'description' => $links['2']->description,
      ];
    }
    $links_module_data->setUris($uris);

    // Define barcode type and value.
    $barcode = new Google_Service_Walletobjects_Barcode();
    $barcode->setType($data['barcode']->type);
    $barcode->setValue($data['barcode']->value);

    // Create wallet object.
    $wob_object = new \Google_Service_Walletobjects_LoyaltyObject();
    $wob_object->setClassId($issuer_id . "." . $class_id);
    $wob_object->setId($issuer_id . "." . $object_id);
    $wob_object->setState('active');
    $wob_object->setVersion(1);
    $wob_object->setBarcode($barcode);

    if (!empty($data['account_id'])) {
      $wob_object->setAccountId($data['account_id']);
    }

    if (!empty($data['account_name'])) {
      $wob_object->setAccountName($data['account_name']);
    }

    if (!empty($data['textModulesData']->header) && !empty($data['textModulesData']->body)) {
      // Define text module data.
      $text_modules_data = [
        [
          'header' => $data['textModulesData']->header,
          'body' => $data['textModulesData']->body,
        ],
      ];

      $wob_object->setTextModulesData($text_modules_data);
    }

    if (!empty($data['messages']['0']->header) && !empty($data['messages']['0']->body)) {
      // Messages to be displayed.
      $messages[] = [
        'header' => $data['messages']['0']->header,
        'body' => $data['messages']['0']->body,
      ];

      if (!empty($data['messages']['1']->header) && !empty($data['messages']['1']->body)) {
        $messages[] = [
          'header' => $data['messages']['1']->header,
          'body' => $data['messages']['1']->body,
        ];
      }
      $wob_object->setMessages($messages);
    }

    $wob_object->setLinksModuleData($links_module_data);

    return $wob_object;
  }

}
