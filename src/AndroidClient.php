<?php

namespace Drupal\digital_wallet_server;

use Drupal\Core\Config\ConfigFactory;

/**
 * Class AndroidClient.
 *
 * @package Drupal\digital_wallet_server
 */
class AndroidClient {

  /**
   * Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * ApplePKPass constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   Config Factory.
   */
  public function __construct(ConfigFactory $configFactory) {
    $this->configFactory = $configFactory;
  }

  /**
   * Function to generate Google Client.
   *
   * @return \Google_Client
   *   Returns Google Client.
   *
   * @throws \Google_Exception
   */
  public function getClient() {
    $config = $this->configFactory->get('digital_wallet_server.android_settings');

    $client = new \Google_Client();

    // Set application name.
    $client->setApplicationName($config->get('application_name'));
    // Set Api scopes.
    $client->setScopes([$config->get('scopes')]);

    // Set your cached access token. Remember to replace $_SESSION with a
    // real database or memcached.
    session_start();

    if (isset($_SESSION['service_token'])) {
      $client->setAccessToken($_SESSION['service_token']);
    }
    // Load the key in json format (you need to download this from the
    // Google API Console when the service account was created.
    $private_key = $config->get('service_account_private_key');
    $client->setAuthConfigFile($private_key);
    if ($client->isAccessTokenExpired()) {
      $client->refreshTokenWithAssertion();
    }
    $_SESSION['service_token'] = $client->getAccessToken();

    return $client;
  }

}
