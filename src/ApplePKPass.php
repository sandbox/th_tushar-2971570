<?php

namespace Drupal\digital_wallet_server;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityManagerInterface;
use ZipArchive;

/**
 * Class PKPass.
 */
class ApplePKPass {

  /**
   * Holds the path to the certificate.
   *
   * @var certPath
   */
  protected $certPath;

  /**
   * Name of the downloaded file.
   *
   * @var name
   */
  protected $name;

  /**
   * Holds the files to include in the .pkpass file.
   *
   * @var files
   */
  protected $files = [];

  /**
   * Holds the remote file urls to include in the .pkpass file.
   *
   * @var remoteFileUrls
   */
  protected $remoteFileUrls = [];

  /**
   * Holds the json.
   *
   * @var json
   */
  protected $json;

  /**
   * Holds the SHAs of the $files array.
   *
   * @var shas
   */
  protected $shas;

  /**
   * Holds the password to the certificate.
   *
   * @var certPass
   */
  protected $certPass = '';

  /**
   * Holds the path to the WWDR Intermediate certificate.
   *
   * @var wwdrCertPath
   */
  protected $wwdrCertPath = '';

  /**
   * Holds the path to a temporary folder.
   *
   * @var tempPath
   */
  protected $tempPath = '/tmp/';

  /**
   * Holds error info if an error occurred.
   *
   * @var sError
   */
  private $sError = '';

  /**
   * Auto-generated uniqid to prevent overwriting other processes pass files.
   *
   * @var uniqid
   */
  private $uniqid = NULL;

  /**
   * Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * ApplePKPass constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   Config Factory.
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   Entity Manager.
   */
  public function __construct(ConfigFactory $configFactory, EntityManagerInterface $entity_manager) {
    $this->configFactory = $configFactory;
    $this->entityManager = $entity_manager;

    $this->tempPath = drupal_realpath('temporary://');
    $this->initialize();
  }

  /**
   * Initialize PKPass with required configurations.
   *
   * @param string|bool $appleWalletId
   *   Identifier for the requested wallet.
   * @param string|bool $certPath
   *   Certificate Path.
   * @param string|bool $certPass
   *   Certificate Password.
   * @param string|bool $json
   *   JSON data.
   */
  public function initialize($appleWalletId = FALSE, $certPath = FALSE, $certPass = FALSE, $json = FALSE) {
    $apple_config = $this->configFactory->get('digital_wallet_server.apple_settings');

    if ($appleWalletId == FALSE) {
      $appleWalletId = \Drupal::config('digital_wallet_server.main_settings')->get('default_apple_wallet');
    }

    $brand_certs = $this->entityManager->getStorage('apple_wallet')->load($appleWalletId);

    $apple_wwdr_file_path = $brand_certs->wwdr_cert_file->value;
    $apple_p12_file_path = $brand_certs->p12_cert_file->value;
    $apple_cert_pass = $brand_certs->cert_file_password->value;
    if (!empty($apple_wwdr_file_path)) {
      $this->wwdrCertPath = $apple_wwdr_file_path;
    }
    else {
      $this->wwdrCertPath = '';
    }
    if ($certPath != FALSE) {
      $this->setCertificate($certPath);
    }
    else {
      if (!empty($apple_p12_file_path)) {
        $this->setCertificate($apple_p12_file_path);
      }
    }
    if ($certPass != FALSE) {
      $this->setCertificatePassword($certPass);
    }
    else {
      if (!empty($apple_cert_pass)) {
        $this->setCertificatePassword($apple_cert_pass);
      }
    }
    if ($json != FALSE) {
      $this->setData($json);
    }
  }

  /**
   * Sets the path to a certificate.
   *
   * @param string $path
   *   Path to Certificate.
   *
   * @return bool
   *   True on success, false if file does not exist.
   */
  public function setCertificate($path) {
    if (file_exists($path)) {
      $this->certPath = $path;

      return TRUE;
    }

    $this->sError = 'Certificate file does not exist.';

    return FALSE;
  }

  /**
   * Sets the certificate's password.
   *
   * @param string $p
   *   Certificate password.
   *
   * @return bool
   *   True or False.
   */
  public function setCertificatePassword($p) {
    $this->certPass = $p;

    return TRUE;
  }

  /**
   * Sets the path to the WWDR Intermediate certificate.
   *
   * @param string $path
   *   Certificate path.
   *
   * @return bool
   *   True or False.
   */
  public function setWwdrCertPath($path) {
    $this->wwdrCertPath = $path;

    return TRUE;
  }

  /**
   * Set the path to the temporary directory.
   *
   * @param string $path
   *   Path to temporary directory.
   *
   * @return bool
   *   True or False.
   */
  public function setTempPath($path) {
    if (is_dir($path)) {
      $this->tempPath = rtrim($path, '/') . '/';

      return TRUE;
    }

    return FALSE;
  }

  /**
   * Set JSON for pass.
   *
   * @param string|array $json
   *   JSON data.
   *
   * @return bool
   *   True or False.
   */
  public function setJson($json) {
    return $this->setData($json);
  }

  /**
   * Set pass data.
   *
   * @param string|array $data
   *   The data array.
   *
   * @return bool
   *   True or False.
   */
  public function setData($data) {
    // Array is passed as input.
    if (is_array($data)) {
      $this->json = json_encode($data);

      return TRUE;
    }

    // JSON string is passed as input.
    if (json_decode($data) !== FALSE) {
      $this->json = $data;

      return TRUE;
    }

    $this->sError = 'This is not a JSON string.';

    return FALSE;
  }

  /**
   * Add a file to the file array.
   *
   * @param string $path
   *   Path to file.
   * @param string $name
   *   Filename to use in pass archive.
   *
   * @return bool
   *   True or False.
   */
  public function addFile($path, $name = NULL) {
    if (file_exists($path)) {
      $name = ($name === NULL) ? basename($path) : $name;
      $this->files[$name] = $path;

      return TRUE;
    }

    $this->sError = sprintf('File %s does not exist.', $path);

    return FALSE;
  }

  /**
   * Add a file from a url to the remote file urls array.
   *
   * @param string $url
   *   URL to file.
   * @param string $name
   *   Filename to use in pass archive.
   *
   * @return bool
   *   True or False.
   */
  public function addRemoteFile($url, $name = NULL) {
    $name = ($name === NULL) ? basename($url) : $name;
    $this->remoteFileUrls[$name] = $url;

    return TRUE;
  }

  /**
   * Create the actual .pkpass file.
   *
   * @param bool $output
   *   Whether to output it directly or return the pass contents as a string.
   *
   * @return bool|string
   *   True or False.
   */
  public function create($output = FALSE) {
    $paths = $this->getTempPaths();

    // Creates and saves the json manifest.
    if (!($manifest = $this->createManifest())) {
      $this->clean();
      $this->sError = 'Error in manifest file creation.';
      return FALSE;
    }

    // Create signature.
    if ($this->createSignature($manifest) == FALSE) {
      $this->clean();
      $this->sError = 'Error in signature creation.';
      return FALSE;
    }

    if ($this->createZip($manifest) == FALSE) {
      $this->clean();
      $this->sError = 'Error in zip file creation.';
      return FALSE;
    }

    // Check if pass is created and valid.
    if (!file_exists($paths['pkpass']) || filesize($paths['pkpass']) < 1) {
      $this->sError = 'Error while creating pass.pkpass. Check your ZIP extension.';
      $this->clean();

      return FALSE;
    }

    // Get contents of generated file.
    $file = file_get_contents($paths['pkpass']);
    $size = filesize($paths['pkpass']);
    $name = basename($paths['pkpass']);
    \Drupal::logger('apple_pkpass')->notice('Size : ' . print_r($size, TRUE));
    // Cleanup.
    $this->clean();

    // Output pass.
    if ($output == TRUE) {
      $fileName = $this->getName() ? $this->getName() : $name;
      if (!strstr($fileName, '.')) {
        $fileName .= '.pkpass';
      }

      header('Content-Description: File Transfer');
      header('Content-Type: application/vnd.apple.pkpass');
      header('Content-Disposition: attachment; filename="' . $fileName . '"');
      header('Content-Transfer-Encoding: binary');
      header('Connection: Keep-Alive');
      header('Expires: 0');
      header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
      header('Pragma: public');
      header('Content-Length: ' . $size);
      ob_end_flush();
      set_time_limit(0);
      echo $file;

      return TRUE;
    }
    return $file;
  }

  /**
   * Returns name value.
   *
   * @return string
   *   Name value.
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Sets the name value.
   *
   * @param string $name
   *   Name value.
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * Checks the error.
   *
   * @param string $error
   *   The error message.
   *
   * @return bool
   *   True or False.
   */
  public function checkError(&$error) {
    if (trim($this->sError) == '') {
      return FALSE;
    }

    $error = $this->sError;

    return TRUE;
  }

  /**
   * Sets the error message.
   *
   * @return string
   *   Returns error text.
   */
  public function getError() {
    return $this->sError;
  }

  /**
   * Sub-function of create().
   *
   * This function creates the hashes for the files and adds them into a json
   * string.
   */
  protected function createManifest() {
    // Creates SHA hashes for all files in package.
    $this->shas['pass.json'] = sha1($this->json);

    $has_icon = FALSE;
    foreach ($this->files as $name => $path) {
      if (strtolower($name) == 'icon.png') {
        $has_icon = TRUE;
      }
      $this->shas[$name] = sha1(file_get_contents($path));
    }

    foreach ($this->remoteFileUrls as $name => $url) {
      if (strtolower($name) == 'icon.png') {
        $has_icon = TRUE;
      }
      $this->shas[$name] = sha1(file_get_contents($url));
    }

    if (!$has_icon) {
      $this->sError = 'Missing required icon.png file.';
      $this->clean();

      return FALSE;
    }

    $manifest = json_encode((object) $this->shas);

    return $manifest;
  }

  /**
   * Converts PKCS7 PEM to PKCS7 DER.
   *
   * @param mixed $signature
   *   The signature data.
   *
   * @return mixed
   *   The encoded signature data.
   */
  protected function convertPemToDer($signature) {
    $begin = 'filename="smime.p7s"';
    $end = '------';
    $signature = substr($signature, strpos($signature, $begin) + strlen($begin));

    $signature = substr($signature, 0, strpos($signature, $end));
    $signature = trim($signature);
    $signature = base64_decode($signature);

    return $signature;
  }

  /**
   * Creates a signature and saves it.
   *
   * @param string $manifest
   *   The manifest data.
   *
   * @return bool
   *   True or False.
   */
  protected function createSignature($manifest) {
    $paths = $this->getTempPaths();

    file_put_contents($paths['manifest'], $manifest);

    if (!$pkcs12 = file_get_contents($this->certPath)) {
      $this->sError = 'Could not read the certificate';
      return FALSE;
    }

    $certs = [];
    if (!openssl_pkcs12_read($pkcs12, $certs, $this->certPass)) {
      $this->sError = 'Invalid certificate file. Make sure you have a ' .
        'P12 certificate that also contains a private key, and you ' .
        'have specified the correct password!';

      return FALSE;
    }
    $certdata = openssl_x509_read($certs['cert']);
    $privkey = openssl_pkey_get_private($certs['pkey'], $this->certPass);

    $openssl_args = [
      $paths['manifest'],
      $paths['signature'],
      $certdata,
      $privkey,
      [],
      PKCS7_BINARY | PKCS7_DETACHED,
    ];

    if (!empty($this->wwdrCertPath)) {
      if (!file_exists($this->wwdrCertPath)) {
        $this->sError = 'WWDR Intermediate Certificate does not exist';

        return FALSE;
      }

      $openssl_args[] = $this->wwdrCertPath;
    }

    call_user_func_array('openssl_pkcs7_sign', $openssl_args);

    $signature = file_get_contents($paths['signature']);
    $signature = $this->convertPemToDer($signature);
    file_put_contents($paths['signature'], $signature);

    return TRUE;
  }

  /**
   * Creates .pkpass (zip archive) file.
   *
   * @param string $manifest
   *   The manifest data.
   *
   * @return bool
   *   True or False.
   */
  protected function createZip($manifest) {
    $paths = $this->getTempPaths();

    // Package file in Zip (as .pkpass).
    $zip = new ZipArchive();
    if (!$zip->open($paths['pkpass'], ZipArchive::CREATE)) {
      $this->sError = 'Could not open ' . basename($paths['pkpass']) . ' with ZipArchive extension.';

      return FALSE;
    }
    $zip->addFile($paths['signature'], 'signature');
    $zip->addFromString('manifest.json', $manifest);
    $zip->addFromString('pass.json', $this->json);
    foreach ($this->files as $name => $path) {
      $zip->addFile($path, $name);
    }
    foreach ($this->remoteFileUrls as $name => $url) {
      $download_file = file_get_contents($url);
      $zip->addFromString($name, $download_file);
    }
    $zip->close();

    return TRUE;
  }

  /**
   * Declares all paths used for temporary files.
   *
   * @return array
   *   Array of temporary paths.
   */
  protected function getTempPaths() {
    // Declare base paths.
    $paths = [
      'pkpass' => 'pass.pkpass',
      'signature' => 'signature',
      'manifest' => 'manifest.json',
    ];

    // If trailing slash is missing, add it.
    if (substr($this->tempPath, -1) != '/') {
      $this->tempPath = $this->tempPath . '/';
    }

    // Generate a unique sub-folder in the tempPath to support generating more
    // passes at the same time without erasing/overwriting each others files.
    if (empty($this->uniqid)) {
      $this->uniqid = uniqid('PKPass', TRUE);

      if (!is_dir($this->tempPath . $this->uniqid)) {
        mkdir($this->tempPath . $this->uniqid);
      }
    }

    // Add temp folder path.
    foreach ($paths as $pathName => $path) {
      $paths[$pathName] = $this->tempPath . $this->uniqid . '/' . $path;
    }

    return $paths;
  }

  /**
   * Removes all temporary files.
   *
   * @return bool
   *   True or False.
   */
  protected function clean() {
    $paths = $this->getTempPaths();

    foreach ($paths as $path) {
      if (file_exists($path)) {
        unlink($path);
      }
    }

    // Remove our unique temporary folder.
    if (is_dir($this->tempPath . $this->uniqid)) {
      rmdir($this->tempPath . $this->uniqid);
    }

    return TRUE;
  }

}
